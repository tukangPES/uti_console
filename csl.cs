﻿
using System;
using System.Collections.Generic;

namespace netCore_console
{
    public class csl
    {
        private readonly Stack<ConsoleColor> _myColorStack;

        public csl()
        {
            _myColorStack = new Stack<ConsoleColor>();
        }

        public csl wr(string instrTextToWrite) => wr(instrTextToWrite, null);
        public csl wrCenter(string instrTextToWrite) => wrCenter(instrTextToWrite, null);
        public csl wrRight(string instrTextToWrite) => wrRight(instrTextToWrite, null);

        public csl wr(string instrTextToWrite, ConsoleColor? iOneTimeColor)
        {
            if (iOneTimeColor != null)
            {
                _ = pushColor();
                _ = changeColor(iOneTimeColor.Value);
            }

            var palingKanan = instrTextToWrite.Substring(instrTextToWrite.Length - 1);

            string strToWrite;
            if ((palingKanan ?? "") == ";")
            {
                strToWrite = instrTextToWrite.Substring(0, instrTextToWrite.Length - 1);
                Console.Write(strToWrite);
            }
            else
            {
                strToWrite = instrTextToWrite;
                Console.WriteLine(strToWrite);
            }

            if (iOneTimeColor != null)
            {
                _ = popColor();
            }

            return this;
        }

        public csl wrCenter(string instrTextToWrite, ConsoleColor? iOneTimeColor)
        {
            var intX = (Console.WindowWidth - instrTextToWrite.Length) / 2;
            var strToWrite = instrTextToWrite.PadLeft(intX + instrTextToWrite.Length, ' ');
            return wr(strToWrite, iOneTimeColor);
        }

        public csl wrRight(string instrTextToWrite, ConsoleColor? iOneTimeColor)
        {
            var strToWrite = instrTextToWrite.PadLeft(Console.WindowWidth, ' ');
            return wr(strToWrite);
        }

        public csl changeColor(ConsoleColor iColor)
        {
            Console.ForegroundColor = iColor;
            return this;
        }

        public csl customPause(int iSecondsToWait)
        {
            for (var intA = iSecondsToWait; intA > 0; intA--)
            {
                System.Threading.Thread.Sleep(1000);
                _ = wr($"{intA}..;");
            }
            _ = wr("");

            return this;
        }

        private csl pushColor()
        {
            _myColorStack.Push(Console.ForegroundColor);
            return this;
        }

        private csl popColor()
        {
            try
            {
                ConsoleColor aColor = _myColorStack.Pop();
                _ = changeColor(aColor);
            }
            catch (Exception)
            {
                //ignore    
            }
            return this;
        }
    }
}
